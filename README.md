# Code from my articles

This repository contains source code, examples and other files
referenced by some of my articles.


## 2020

- `attrs-dataclasses-pydantic`: https://stefan.sofa-rockers.org/2020/05/30/attrs-dataclasses-pydantic/


## 2013

- `subproc_handling`: https://stefan.sofa-rockers.org/2013/08/15/handling-sub-process-hierarchies-python-linux-os-x/


## 2012

- `pyzmq`:
  - https://stefan.sofa-rockers.org/2012/02/01/designing-and-testing-pyzmq-applications-part-1/
  - https://stefan.sofa-rockers.org/2012/02/07/designing-and-testing-pyzmq-applications-part-2/
  - https://stefan.sofa-rockers.org/2012/02/15/designing-and-testing-pyzmq-applications-part-3/
