import os.path
import subprocess

import pytest


pytestmark = pytest.mark.system


def test_pongproc():
    filename = os.path.join('example_app', 'test', 'data', 'pongproc.out')
    expected = open(filename).read()

    output = subprocess.check_output(['python', 'example_app/pongproc.py'],
                                     universal_newlines=True)

    assert output == expected
