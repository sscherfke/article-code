import pytest
import zmq

from test.support import ProcessTest, make_sock
import pongproc


pytestmark = pytest.mark.process

host = '127.0.0.1'
port = 5678


class TestProngProc(ProcessTest):
    """Communication test for the Platform Manager process."""

    def setup_method(self, method):
        """
        Creates and starts a pp process and sets up sockets to communicate
        with it.

        """
        self.context = zmq.Context()

        # Mimics the ping process
        self.req_sock = make_sock(self.context, zmq.REQ,
                                  connect=(host, port))

        self.pp = pongproc.PongProc((host, port))
        self.pp.start()

    def teardown_method(self, method):
        """
        Sends a kill message to the pp and waits for the process to terminate.

        """
        # Send a stop message to the prong process and wait until it joins
        self.req_sock.send_multipart([b'["plzdiekthxbye", null]'])
        self.pp.join()

        # Assert that no more messages are in the pipe
        pytest.raises(zmq.ZMQError, self.req_sock.recv_multipart)

        self.req_sock.close()

    def test_ping(self):
        """Tests a ping-pong sequence."""
        yield ('send', self.req_sock, [], ['ping', 1])

        reply = yield ('recv', self.req_sock)
        assert reply == [['pong', 1]]
