from inspect import isfunction, isgeneratorfunction


def pytest_pycollect_makeitem(collector, name, obj):
    """
    Collects all instance methods that are generators and returns them as
    normal function items.

    """
    if collector.funcnamefilter(name) and hasattr(obj, '__call__'):
        if isfunction(obj) or isgeneratorfunction(obj):
            return collector._genfunctions(name, obj)


def pytest_runtest_call(item):
    """
    Passes the test generator (``item.obj``) to the ``run()`` method of the
    generator's instance. This method should be inherited from
    :class:`test.support.ProcessTest`.

    """
    if isgeneratorfunction(item.obj):
        item.obj.__self__.run(item.obj)
    else:
        item.runtest()
