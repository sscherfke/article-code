import json
import pathlib

import matplotlib.pyplot as plt
import numpy as np


for f in sorted(pathlib.Path().glob('*.json')):
    print(f)
    results = json.load(f.open())
    title = results['title']
    labels, duration, mem = list(zip(*results['data']))
    duration = np.array(duration)
    duration /= duration.max()
    mem = np.array(mem)
    mem /= mem.max()

    y_pos = np.arange(len(labels))  # the label locations
    height = 0.35  # the width of the bars
    fig, ax = plt.subplots()
    rects1 = ax.barh(y_pos - height/2, duration, height, label='Duration')
    rects2 = ax.barh(y_pos + height/2, mem, height, label='Memory')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(labels)
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.legend(loc='upper right')
    ax.set_xticks([])
    ax.set_title(title)
    plt.tight_layout()
    plt.savefig(f.with_suffix('.png'))
    plt.savefig(f.with_suffix('.svg'))
