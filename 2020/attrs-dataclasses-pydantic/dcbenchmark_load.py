# pylint: disable=invalid-name,no-self-use,redefined-outer-name,wrong-import-position
from datetime import datetime
import gc
import json
import os
import time
import psutil
process = psutil.Process(os.getpid())

from attr import dataclass, attrib
from pydantic import BaseModel, Field
import cattr


DATA = {
    'name': 'spam',
    'child': {
        'x': 23,
        'y': '42',  # sic!
        'd': '2020-05-04T13:37:00',
    },
}


def attr1():
    """Attr convert, no validation"""
    @dataclass(slots=True, frozen=True)
    class Child:
        x: int = attrib(converter=int)
        y: int = attrib(converter=int)
        d: datetime = attrib(converter=datetime.fromisoformat)

    @dataclass(slots=True, frozen=True)
    class Parent:
        name: str
        child: Child = attrib(converter=lambda c: Child(**c))

    def run():
        return Parent(**DATA)

    return run


def attr2():
    """Attr convert, with validation"""
    @dataclass(slots=True, frozen=True)
    class Child:
        x: int = attrib(converter=int)
        y: int = attrib(converter=int)
        d: datetime = attrib(converter=datetime.fromisoformat)

        @x.validator
        def _check(self, attribute, value):
            if value < 0:
                raise ValueError('x must not be negative')

        @y.validator
        def _check(self, attribute, value):
            if value < 0:
                raise ValueError('y must not be negative')

    @dataclass(slots=True, frozen=True)
    class Parent:
        name: str = attrib()
        child: Child = attrib(converter=lambda c: Child(**c))

        @name.validator
        def _check(self, attribute, value):
            if len(value) > 10:
                raise ValueError('name must have <= 10 characters')

    def run():
        return Parent(**DATA)

    return run


def cattr1():
    """Cattr convert, no validation"""
    @dataclass(slots=True, frozen=True)
    class Child:
        x: int
        y: int
        d: datetime

    @dataclass(slots=True, frozen=True)
    class Parent:
        name: str
        child: Child

    converter = cattr.Converter()
    converter.register_unstructure_hook(datetime, lambda dt: dt.isoformat())
    converter.register_structure_hook(
        datetime, lambda ts, _: datetime.fromisoformat(ts)
    )

    def run():
        return converter.structure(DATA, Parent)

    return run


def pydantic1():
    """Pydantic, no ext. validation"""
    class Child(BaseModel):
        x: int
        y: int
        d: datetime


    class Parent(BaseModel):
        name: str
        child: Child

    def run():
        return Parent(**DATA)

    return run


def pydantic2():
    """Pydantic, with ext. validation"""
    class Child(BaseModel):
        x: int = Field(..., ge=0)
        y: int = Field(..., ge=0)
        d: datetime


    class Parent(BaseModel):
        name: str = Field(..., max_length=10)
        child: Child

    def run():
        return Parent(**DATA)

    return run


funcs = [
    attr1,
    attr2,
    cattr1,
    pydantic1,
    pydantic2,
]

results = {
    'title': 'Loading nested classes',
    'data': [],
}
for f in funcs:
    run = f()
    comment = f.__doc__
    gc.collect()
    time.sleep(2)

    start_rss = process.memory_info().rss
    start_t = time.perf_counter()
    l = [run() for _ in range(1_000_000)]
    end_t = time.perf_counter()
    end_rss = process.memory_info().rss
    duration_s = end_t - start_t
    mem_mib = (end_rss - start_rss) / 1024 ** 2
    results['data'].append([comment, duration_s, mem_mib])
    assert isinstance(l[0].child.x, int)
    assert isinstance(l[0].child.y, int)
    assert isinstance(l[0].child.d, datetime)
    print(comment)
    print(f'time: {duration_s:.1f}s')
    print(f'rss:  {mem_mib:.1f}MiB')
    print()
    del l
json.dump(results, open(__file__.replace('.py', '.json'), 'w'))
