# pylint: disable=invalid-name,no-self-use,redefined-outer-name,wrong-import-position
from datetime import datetime
import gc
import json
import os
import time

from attr import dataclass, attrib, asdict, has
from pydantic import BaseModel, Field
import cattr
import psutil


process = psutil.Process(os.getpid())


DATA = {
    'name': 'spam',
    'child': {
        'x': 23,
        'y': '42',  # sic!
        'd': '2020-05-04T13:37:00',
    },
}


def attr1():
    """Attr"""
    @dataclass(slots=True, frozen=True)
    class Child:
        x: int = attrib(converter=int)
        y: int = attrib(converter=int)
        d: datetime = attrib(converter=datetime.fromisoformat)

    @dataclass(slots=True, frozen=True)
    class Parent:
        name: str
        child: Child = attrib(converter=lambda c: Child(**c))

    class Encoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, datetime):
                return o.isoformat()
            if has(o):
                return asdict(o)
            return super().default(o)

    encoder = Encoder()

    def prepare():
        return Parent(**DATA)

    def run(inst):
        return encoder.encode(inst)

    return prepare, run


def cattr1():
    """Cattr"""
    @dataclass(slots=True, frozen=True)
    class Child:
        x: int
        y: int
        d: datetime

    @dataclass(slots=True, frozen=True)
    class Parent:
        name: str
        child: Child

    converter = cattr.Converter()
    converter.register_unstructure_hook(datetime, lambda dt: dt.isoformat())
    converter.register_structure_hook(
        datetime, lambda ts, _: datetime.fromisoformat(ts)
    )

    def prepare():
        return converter.structure(DATA, Parent)

    def run(inst):
        return json.dumps(converter.unstructure(inst))

    return prepare, run


def pydantic1():
    """Pydantic"""
    class Child(BaseModel):
        x: int
        y: int
        d: datetime


    class Parent(BaseModel):
        name: str
        child: Child

    def prepare():
        return Parent(**DATA)

    def run(inst):
        return inst.json()

    return prepare, run


funcs = [
    attr1,
    cattr1,
    pydantic1,
]

results = {
    'title': 'Dumping nested classes',
    'data': [],
}
for f in funcs:
    prepare, run = f()
    inst = prepare()
    comment = f.__doc__
    gc.collect()
    time.sleep(2)

    start_rss = process.memory_info().rss
    start_t = time.perf_counter()
    l = [run(inst) for _ in range(1_000_000)]
    end_t = time.perf_counter()
    end_rss = process.memory_info().rss
    duration_s = end_t - start_t
    mem_mib = (end_rss - start_rss) / 1024 ** 2
    results['data'].append([comment, duration_s, mem_mib])
    print(comment)
    print(f'time: {duration_s:.1f}s')
    print(f'rss:  {mem_mib:.1f}MiB')
    print()
    del l
json.dump(results, open(__file__.replace('.py', '.json'), 'w'))
