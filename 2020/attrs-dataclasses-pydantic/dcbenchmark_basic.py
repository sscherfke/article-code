# pylint: disable=invalid-name,no-self-use,wrong-import-position
import gc
import json
import os
import time
import psutil
process = psutil.Process(os.getpid())

import dataclasses

import attr
import pydantic


@dataclasses.dataclass
class DcDataclasses1:
    """Dataclasses: dataclass()"""
    id: int
    name: str
    value: float


@attr.s
class DcAttrs1:
    """Attrs: attr.s() + attr.ib()"""
    id = attr.ib()
    name = attr.ib()
    value = attr.ib()


@attr.dataclass
class DcAttrs2:
    """Attrs: dataclass()"""
    id: int
    name: str
    value: float


@attr.s
class DcAttrs3:
    """Attrs: attr.s(slots=True) + attr.ib()"""
    id = attr.ib()
    name = attr.ib()
    value = attr.ib()


@attr.dataclass(slots=True)
class DcAttrs4:
    """Attrs: dataclass(slots=True)"""
    id: int
    name: str
    value: float


@attr.dataclass(slots=True)
class DcAttrs5:
    """Attrs: dataclass(slots=True) + attrib(converter)"""
    id: int = attr.ib(converter=int)
    name: str = attr.ib(converter=str)
    value: float = attr.ib(converter=float)


@attr.dataclass(slots=True)
class DcAttrs6:
    """Attrs: dataclass(slots=True) + attrib(converter, validator)"""
    id: int = attr.ib(converter=int)
    name: str = attr.ib(converter=str)
    value: float = attr.ib(converter=float)

    @id.validator
    def _check_id(self, attribute, value):
        if value < 0:
            raise ValueError('id must not be negative')

    @name.validator
    def _check_name(self, attribute, value):
        if len(value) > 50:
            raise ValueError('name must be shorter than 51 characters')

    @value.validator
    def _check_value(self, attribute, value):
        if value < 0:
            raise ValueError('value must not be negative')


class DcPydantic1(pydantic.BaseModel):
    """Pydantic: BaseModel"""
    id: int
    name: str
    value: float


class DcPydantic2(pydantic.BaseModel):
    """Pydantic: BaseModel with slots"""
    __slots__ = ('id', 'name', 'value')
    id: int
    name: str
    value: float


@pydantic.dataclasses.dataclass
class DcPydantic3:
    """Pydantic: dataclass()"""
    id: int
    name: str
    value: float


class DcPydantic4(pydantic.BaseModel):
    """Pydantic: BaseModel + ext. validation"""
    id: int = pydantic.Field(..., ge=0)
    name: str = pydantic.Field(..., max_length=50)
    value: float = pydantic.Field(..., ge=0)


@pydantic.dataclasses.dataclass
class DcPydantic5:
    """Pydantic: dataclass() + ext. validation"""
    id: int = pydantic.Field(..., ge=0)
    name: str = pydantic.Field(..., max_length=50)
    value: float = pydantic.Field(..., ge=0)


classes = [
    DcDataclasses1,
    DcAttrs1,
    DcAttrs2,
    DcAttrs3,
    DcAttrs4,
    DcAttrs5,
    DcAttrs6,
    DcPydantic1,
    DcPydantic2,
    DcPydantic3,
    DcPydantic4,
    DcPydantic5,
]

results = {
    'title': 'Basic class instantiation',
    'data': [],
}
for cls in classes:
    comment = cls.__doc__
    gc.collect()
    time.sleep(2)

    start_rss = process.memory_info().rss
    start_t = time.perf_counter()
    l = [cls(id=i, name=f'spam{i}', value=i * 0.2) for i in range(1_000_000)]
    end_t = time.perf_counter()
    end_rss = process.memory_info().rss
    duration_s = end_t - start_t
    mem_mib = (end_rss - start_rss) / 1024 ** 2
    results['data'].append([comment, duration_s, mem_mib])
    print(comment)
    print(f'time: {duration_s:.1f}s')
    print(f'rss:  {mem_mib:.1f}MiB')
    print()
    del l
json.dump(results, open(__file__.replace('.py', '.json'), 'w'))
