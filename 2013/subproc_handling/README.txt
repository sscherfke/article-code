Various examples and tests on how child processes can (or cannot) be terminated
on Linux, OS X and Windows.
