"""
A SIGINT is not forwarded to child processes if you send it into a
subprocess.

Works on:

- Linux
- OS X

"""
import signal
import subprocess
import sys
import time


def parent():
    proc = subprocess.Popen(['python', 'sigint.py', 'child'])
    try:
        time.sleep(60)
    except KeyboardInterrupt:
        # Uncomment this line to see that SIGINT is not forwarded
        # automatically.
        proc.send_signal(signal.SIGINT)
        print('parent waiting for child')
        proc.wait()
        print('parent terminating')


def child():
    try:
        time.sleep(60)
    except KeyboardInterrupt:
        print('child terminating')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        proc = subprocess.Popen(['python', 'sigint.py', 'parent'])
        time.sleep(1)
        print('Sending SIGINT to parent')
        proc.send_signal(signal.SIGINT)
        print('Waiting for parent')
        proc.wait()
    elif sys.argv[1] == 'parent':
        parent()
    elif sys.argv[1] == 'child':
        child()
