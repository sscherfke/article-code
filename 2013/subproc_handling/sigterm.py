"""
A SIGTERM is not forwarded to child processes if you send it into a
subprocess.

``proc.send_signal(signal.SIGTERM)`` is equivalent to ``proc.terminate()``.

Works on:

- Linux
- OS X
- Windows?

"""
import os
import signal
import subprocess
import sys
import time


WIN = (sys.platform == 'win32')
kwargs = {}
if WIN:
    kwargs['creationflags'] = subprocess.CREATE_NEW_PROCESS_GROUP


def parent():
    proc = subprocess.Popen(['python', 'sigterm.py', 'child'], **kwargs)

    def handler(signum, frame=None):
        print('parent received SIGTERM')
        if WIN:
            # import ctypes
            # ctypes.windll.kernel32.GenerateConsoleCtrlEvent(signal.CTRL_BREAK_EVENT, proc.pid)
            os.kill(proc.pid, signal.CTRL_BREAK_EVENT)
        else:
            proc.terminate()

        print('parent waiting for child')
        proc.wait()
        print('parent terminating')
        sys.exit(0)

    if WIN:
        import ctypes
        handler = ctypes.WINFUNCTYPE(ctypes.c_int, ctypes.c_uint)(handler)
        ctypes.windll.kernel32.SetConsoleCtrlHandler(handler, True)
        # signal.signal(signal.SIGBREAK, handler)
    else:
        signal.signal(signal.SIGTERM, handler)
    time.sleep(60)


def child():
    def handler(signum, frame=None):
        print('child terminating')
        sys.exit(0)

    if WIN:
        import ctypes
        handler = ctypes.WINFUNCTYPE(ctypes.c_int, ctypes.c_uint)(handler)
        ctypes.windll.kernel32.SetConsoleCtrlHandler(handler, True)
        # signal.signal(signal.SIGBREAK, handler)
    else:
        signal.signal(signal.SIGTERM, handler)
    time.sleep(60)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        proc = subprocess.Popen(['python', 'sigterm.py', 'parent'], **kwargs)
        time.sleep(1)
        print('Sending SIGTERM to parent')
        if WIN:
            # import ctypes
            # ctypes.windll.kernel32.GenerateConsoleCtrlEvent(signal.CTRL_BREAK_EVENT, proc.pid)
            os.kill(proc.pid, signal.CTRL_BREAK_EVENT)
        else:
            proc.terminate()
        print('Waiting for parent')
        proc.wait()
    elif sys.argv[1] == 'parent':
        parent()
    elif sys.argv[1] == 'child':
        child()
