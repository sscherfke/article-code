"""
SIGKILL cannot be caught and there is no way to forward it to child
processes.

On Linux, ``proc.send_signal(signal.SIGKILL)`` is equivalent to
``proc.kill()``. On Windows, ``proc.kill()`` will send a ``SIGTERM``.

Works on:

- Linux
- OS X

"""
import signal
import subprocess
import sys
import time


def parent():
    proc = subprocess.Popen(['python', 'sigkill.py', 'child'])
    time.sleep(5)
    proc.wait()
    print('Parent was not killed.')


def child():
    time.sleep(5)
    print('Child was not killed.')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        proc = subprocess.Popen(['python', 'sigkill.py', 'parent'])
        time.sleep(1)
        print('Sending SIGKILL to parent')
        proc.send_signal(signal.SIGKILL)
        print('Waiting for parent')
        proc.wait()
    elif sys.argv[1] == 'parent':
        parent()
    elif sys.argv[1] == 'child':
        child()
